const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')

const loginRouter = require('./routers/login')
const userRouter = require('./routers/user')
const authMiddleware = require('./middlewares/auth')

const PORT = 8800
const app = express()

//db initialization
require('./database').dbInit()

app.set("view engine","jade")
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


app.use('/login', loginRouter.router)
app.use('/user', authMiddleware.validateAdmin,userRouter.router)

app.use((err,req,res,next) => {
    const status = err.status
    let errorMessage = 'Internal Server Error'
    
    switch(status){
        case 400: 
            errorMessage = 'Bad Request'
            break
        case 401:
            errorMessage = 'Unauthorized Request!'
            break
    }
    res.status(status).json({message: errorMessage})
})

app.listen(PORT, () => {
    console.log(`Server is listening at port ${PORT}`)
})

