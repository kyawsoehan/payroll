const Admin = require('../models/Admin').Admin

function FindOneAdmin(nric) {
    
    return Admin.findOne({nric})
            .then(data => {
                console.log(data)
                return {
                    err: null,
                    data
                }
            })
            .catch(e => {
                return {
                    err: e,
                    data :null
                }
            })

}

module.exports = {
    FindOneAdmin
}