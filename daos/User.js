const mongoose = require('mongoose')


const User = require('../models/User').User

function FindAllUsers(){
    return User.find({})
            .then(data => {
                return {
                    data,
                    error: null
                }
            }).catch(e => {
                return {
                    error: e,
                    data: null
                }
            })
            
}

function CreateOneUser(user) {
    user._id = mongoose.Types.ObjectId()
    return User.create(user)
            .then(result => {
                console.log(result)
                return {
                    error: null
                }
            })
            .catch(e => {
                return {
                    error: e
                }
            })
}

function DeleteUserById(id){
    return User.deleteOne({_id: id})
            .then(result => {
                console.log(result)
                return {
                    error: null
                }
            })
            .catch(e => {
                return {
                    error: e
                }
            })
}

module.exports = {
    FindAllUsers,
    CreateOneUser,
    DeleteUserById
}