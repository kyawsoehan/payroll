const mongoose = require('mongoose')

function dbInit(){
  mongoose.connect('mongodb://localhost/itaps');
  const db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open', function callback () {
    console.log("Database connected");
  });
}

module.exports = {
  dbInit
}

