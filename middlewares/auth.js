const tokenUtil = require('../utility/token')

async function validateAdmin(req,res,next){
    if (req.cookies && req.cookies.token){
        const {error, info} = await tokenUtil.validateToken(req.cookies.token)
            if (error){
                res.cookie('token', '', {expires: Date.now()})
                res.redirect('/login')
                return
            }
            req.info = info
            next()

    }else {
        res.redirect('/login')
    }
}


module.exports = {
    validateAdmin
}