const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ObjectId = Schema.ObjectId

const AdminSchema = new Schema({
    _id: ObjectId,
    nric: {
        type: String,
        required: true
    },
    hashedPassword: {
        type: String,
        required: true
    }
})

const Admin = mongoose.model('Admins', AdminSchema, 'Admins')

module.exports = {
    Admin
}