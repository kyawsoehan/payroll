const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ObjectId = Schema.ObjectId

const UserSchema = new Schema({
    _id: ObjectId,
    name: {
        type: String,
        required: true
    },
    nric: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    }
})

const User = mongoose.model('Users', UserSchema, 'Users')

module.exports = {
    User
}