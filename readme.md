## Technology Stack Info
- database - mongodb
- web framework - express

## Installation
Use npm for library installation
```bash
    npm install
```
Go to the project root folder and type to generate the hashed password for "admin123" and store the hashed password in mongodb collections called "Admins"
```bash
    node initial-hash
```
properties for "Admins" Collection

-nric = "admin"
-hashedPassword = "output from the above command"

## Usage
Go to the project root folder and run via
```bash
    node app.js
```

- Login page - localhost:8800/login 
- User List View Page - localhost:8800/user
- Add user page - localhost:8800/user
- Delete User - localhost:8800/user/deleteuser/{userId}