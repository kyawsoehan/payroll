const express = require('express')
const bcrypt = require('bcrypt')

const adminDao = require('../daos/Admin')
const tokenUtil = require('../utility/token')

const router = express.Router()

router.get('/', (req,res) => {
    res.render('login')
})

router.post('/', async (req,res,next) => {
    const {loginId, password} = req.body
    if (loginId && password){
        const {err, data} = await adminDao.FindOneAdmin(loginId)
        if (err){
            res.json(err)
            return
        }else if (data === null){
            next({status: 401})
        }
        bcrypt.compare(password, data.hashedPassword, (error,result) => {
            if (result === true){
                const payload = {
                    id: data._id,
                    nric: data.nric
                }
                const token = tokenUtil.generateToken(payload)
                res.cookie('token', token)
                res.redirect('/user')
            }else {
                next({status: 401})
            }
        })

    }else {
        res.redirect('/login')
    }
})

module.exports = {
    router
}