const express = require('express')

const userDao = require('../daos/User')

const router = express.Router()

router.get('/', async (req,res) => {
    const {error, data} = await userDao.FindAllUsers()
    if (error){
        next({status: 500})
        return
    }
    res.render('user-list', {users: data})
})

router.get('/adduser', (req,res) => {
    res.render('add-user')
})

router.get('/deleteuser/:id', async (req,res,next) => {
    const {id} = req.params
    if (id){
        const {error} = await userDao.DeleteUserById(id)
        if (error){
            next({status: 500})
            return
        }

        res.redirect('/user')

    }else {
        next({status: 400})
    }
})

router.post('/adduser', async (req,res,next) => {
    const {name, nric, age} = req.body
    if (!name || !nric || isNaN(age)){
        next({status: 400})
        return
    }
    const user = {
        name,
        age,
        nric
    }
    const {error} = await userDao.CreateOneUser(user)
    console.log('Error is ',error)
    if (error){
        next({status: 500})
    }else{
        res.redirect('/user')
        return
    }
})

module.exports = {
    router
}