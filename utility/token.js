const jwt = require('jsonwebtoken')

function generateToken(payload){
    return jwt.sign(payload, 'my secret')
}

function validateToken(token){
    return jwt.verify(token, 'my secret', (err,decoded) => {
        if (err){
            return {
                error: err,
                info: decoded
            }
        }else {
            return {
                error: null,
                info: decoded
            }
        }
    })
}

module.exports = {
    generateToken,
    validateToken
}